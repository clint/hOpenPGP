-- Version.hs: static hOpenPGP version string
-- Copyright © 2024  Clint Adams
-- This software is released under the terms of the Expat license.
-- (See the LICENSE file).
module Codec.Encryption.OpenPGP.Version
  ( version
  ) where

import Data.Version (showVersion)
import qualified Paths_hOpenPGP as Paths

version :: String
version = showVersion Paths.version
